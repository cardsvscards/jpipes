import java.io.*; 
import java.net.*; 

public class Client 
{

	public static void main(String[] args) throws Exception
	{
		Client CLIENT = new Client();
		CLIENT.run(); 	
	}
	
		//Insted of doing a try-catch block, throws an Exception
		public void run() throws Exception
		{
			Socket SOCK = new Socket("LocalHost", 444); 
			//PS sends messages to the SERVER 
			PrintStream PS = new PrintStream(SOCK.getOutputStream());  
			PS.println("Hello to SERVER from CLIENT");
			
			//CLIENT listens to what SERVER outputs 
			InputStreamReader IR = new InputStreamReader(SOCK.getInputStream());
			BufferedReader BR = new BufferedReader(IR); 
			
			//Reads the first line of the server's message, and prints it out.
			String MESSAGE = BR.readLine(); 
			System.out.println(MESSAGE); 
		}
}

/*Run Server program first to get it working and listening for the client, 
 * and then run the Client program*/
