import java.io.*; 
import java.net.*;

/*
 * 
 */
public class Server 
{
	//in the book, they have an exception in case there's no input/output
	public void run() throws Exception 
	{
		ServerSocket SERVSOCK = new ServerSocket(444); 
		Socket SOCK = SERVSOCK.accept(); 
		InputStreamReader IR = new InputStreamReader(SOCK.getInputStream()); 
		BufferedReader BR = new BufferedReader(IR);
		
		String MESSAGE = BR.readLine(); 
		System.out.println(MESSAGE);
		
		if(MESSAGE != null)
		{
			PrintStream PS = new PrintStream(SOCK.getOutputStream());
			PS.println("Message received!");
		}
	}
	
	public static void main(String[]args) throws Exception
	{
		 SOCK_1_SERVER SERVER = new SOCK_1_SERVER(); 
		 SERVER.run();
	}

}
